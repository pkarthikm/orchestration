package com.zsassociates.platformfoundation.service.orchestration.controller;

import com.zsassociates.platformfoundation.service.orchestration.config.SwaggerConfig;
import com.zsassociates.platformfoundation.service.orchestration.model.*;
import com.zsassociates.platformfoundation.service.orchestration.service.*;
import io.swagger.annotations.Api;
import org.activiti.engine.*;
import org.activiti.engine.task.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

@Api(tags = { SwaggerConfig.TAG_APPROVE })
@RestController
@RequestMapping("/approval")
public class ApprovalController {
    private static final Logger logger = LoggerFactory.getLogger(ApprovalController.class);

    @Autowired
    private ProcessService processService;

    @Autowired
    private TaskUpdateService taskService;

    @RequestMapping(value = "/start-process", method = RequestMethod.POST)
    public ProcessInstanceRepresentation startProcess(@RequestBody StartProcessRequest startProcessRequest) {

        if( null == startProcessRequest)
            throw new IllegalArgumentException("This method takes non null arguments");
        String key = startProcessRequest.getKey();
        if(StringUtils.isEmpty(key))
            throw new IllegalArgumentException("Key cannot be null");
        Object data = startProcessRequest.getData();
        if(null == data)
            throw new IllegalArgumentException("Text to be reviewed cannot be empty");
        return processService.startProcess(startProcessRequest);
    }

    @RequestMapping(value = "/list-processes", method = RequestMethod.GET)
    public List<ProcessInstanceRepresentation> listProcessInstances(@RequestParam(value="isActive", required = false, defaultValue = "false") Boolean isActive) {
        return processService.listProcessInstances(isActive);
    }

    @RequestMapping(value = "/get-process/{processInstanceId}", method = RequestMethod.GET)
    public ProcessInstanceRepresentation getProcess(@PathVariable String processInstanceId) {
        if(StringUtils.isEmpty(processInstanceId))
            throw new IllegalArgumentException("Process Instance Id cannot be null");
        return processService.getProcess(processInstanceId);
    }

    @RequestMapping(value = "/list-tasks", method = RequestMethod.GET)
    public List<TaskRepresentation> listTasks() {
        return taskService.listTasks();
    }

    @RequestMapping(value = "/get-tasks/{processInstanceId}", method = RequestMethod.GET)
    public List<TaskRepresentation> getTasks(@PathVariable String processInstanceId) {
        if(StringUtils.isEmpty(processInstanceId))
            throw new IllegalArgumentException("Process Instance Id cannot be null");
        return taskService.getTasks(processInstanceId);
    }

    @RequestMapping(value = "/get-tasks-by-assignee/{assignee}", method = RequestMethod.GET)
    public List<TaskRepresentation> getTasksByAssignee(@PathVariable String assignee) {
        if(StringUtils.isEmpty(assignee))
            throw new IllegalArgumentException("Process Instance Id cannot be null");
        return taskService.getTasksByAssignee(assignee);
    }

    @RequestMapping(value = "/get-historic-process/{processInstanceId}", method = RequestMethod.GET)
    public HistoricProcessRepresentation getHistoricProcess(@PathVariable String processInstanceId) {
        if(StringUtils.isEmpty(processInstanceId))
            throw new IllegalArgumentException("Process Instance Id cannot be null");

        return processService.getHistoricProcess(processInstanceId);
    }

    @RequestMapping(value = "/get-historic-process-variables/{processInstanceId}", method = RequestMethod.GET)
    public Map<String, Object> getHistoricProcessVariables(@PathVariable String processInstanceId) {
        if(StringUtils.isEmpty(processInstanceId))
            throw new IllegalArgumentException("Process Instance Id cannot be null");

        return processService.getHistoricProcessVariables(processInstanceId);
    }

    @RequestMapping(value = "/get-historic-tasks/{processInstanceId}", method = RequestMethod.GET)
    public List<HistoricTaskRepresentation> getHistoricTasks(@PathVariable String processInstanceId) {
        if(StringUtils.isEmpty(processInstanceId))
            throw new IllegalArgumentException("Process Instance Id cannot be null");

        return taskService.getHistoricTasks(processInstanceId);
    }

    @RequestMapping(value = "/get-historic-tasks-by-assignee/{assignee}", method = RequestMethod.GET)
    public List<HistoricTaskRepresentation> getHistoricTasksByAssignee(@PathVariable String assignee) {
        if(StringUtils.isEmpty(assignee))
            throw new IllegalArgumentException("Assignee cannot be null");

        return taskService.getHistoricTasksByAssignee(assignee);
    }

    @RequestMapping(value = "/get-historic-variables/{processInstanceId}", method = RequestMethod.GET)
    public List<HistoricVariableRepresentation> getHistoricVariables(@PathVariable String processInstanceId,
                                                                     @RequestParam(value="isActive", required = false, defaultValue = "false") Boolean includeTaskVariables) {
        if(StringUtils.isEmpty(processInstanceId))
            throw new IllegalArgumentException("Process Instance Id cannot be null");

        return taskService.getHistoricVariables(processInstanceId, !includeTaskVariables);
    }

    @RequestMapping(value = "/get-historic-task/{taskId}", method = RequestMethod.GET)
    public HistoricTaskRepresentation getHistoricTask(@PathVariable String taskId) {
        if(StringUtils.isEmpty(taskId))
            throw new IllegalArgumentException("Task Id cannot be null");

        return taskService.getHistoricTask(taskId);
    }

    @RequestMapping(value = "/review-task/{taskId}", method = RequestMethod.GET)
    public ResponseEntity<?> reviewTask(@PathVariable String taskId,
                                        @RequestParam(value="isApproved", required = false, defaultValue = "false") Boolean isApproved) {
        //Validate the task Id
        if(StringUtils.isEmpty(taskId))
            return new ResponseEntity<>("Null or Empty task id", HttpStatus.BAD_REQUEST);

        //Validate the task
        Task task = null;
        try {
            task = taskService.getTask(taskId);
        } catch(ActivitiException ae) {
            return new ResponseEntity<>("The query resulted in more than one entities.", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if(null == task)
            return new ResponseEntity<>("No active task exists with the given task id", HttpStatus.BAD_REQUEST);

        taskService.reviewTask(task, isApproved);
        return new ResponseEntity<>("User task completed: " + taskId, HttpStatus.OK);
    }

    @RequestMapping(value = "/delete/{processInstanceId}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteProcessInstance(@PathVariable String processInstanceId) {
        if(StringUtils.isEmpty(processInstanceId))
            return new ResponseEntity<>("Null or Empty process instance id", HttpStatus.BAD_REQUEST);
        try {
            processService.deleteProcess(processInstanceId);
        } catch(IllegalArgumentException iae) {
            return new ResponseEntity<>("Null or Empty process instance id", HttpStatus.BAD_REQUEST);
        } catch(ActivitiObjectNotFoundException aonfe) {
            return new ResponseEntity<>("Process not found to delete", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>("Process Deleted successfully", HttpStatus.OK);
    }

    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteAllProcessInstances() {
        List<String> notDeletedProcessInstances = processService.deleteAllProcessInstances();
        if(null == notDeletedProcessInstances || notDeletedProcessInstances.isEmpty())
            return new ResponseEntity<>("All Process Instances deleted successfully", HttpStatus.OK);
        else {
            String undeletedPids = notDeletedProcessInstances.stream().collect(Collectors.joining());
            return new ResponseEntity<>("These Process Instances could not be deleted " + undeletedPids,
                    HttpStatus.OK);
        }
    }

    /*private ExecutionEntity getRootExecution(String executionId) {
        if(StringUtils.isEmpty(executionId))
            return null;
        ExecutionEntity executionEntity = (ExecutionEntity) runtimeService.createExecutionQuery().executionId(executionId).singleResult();
        if (executionEntity == null) {
            return null;
        }
        String parentId = executionEntity.getParentId();
        boolean isParentEmpty = StringUtils.isEmpty(parentId);
        String superExecutionId = executionEntity.getSuperExecutionId();
        boolean isSuperEmpty = StringUtils.isEmpty(superExecutionId);
        if (!isParentEmpty) {
            return getRootExecution(parentId);
        } else if (!isSuperEmpty) {
            return getRootExecution(superExecutionId);
        } else {
            return executionEntity;
        }
    }*/
}
