package com.zsassociates.platformfoundation.service.orchestration.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.Map;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class HistoricProcessRepresentation {
    private String id;
    private String businessKey;
    private String processDefinitionId;
    private String processDefinitionName;
    private String processDefinitionKey;
    private Integer processDefinitionVersion;
    private String deploymentId;
    private Date startTime;
    private Date endTime;
    private Long durationInMillis;
    private String endActivityId;
    private String startUserId;
    private String startActivityId;
    private String deleteReason;
    private String superProcessInstanceId;
    private String tenantId;
    private String name;
    private String description;
    private Map<String, Object> processVariables;
}
