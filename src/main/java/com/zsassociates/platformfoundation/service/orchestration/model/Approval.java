package com.zsassociates.platformfoundation.service.orchestration.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY,property = "@class")
public class Approval {

    private static final ObjectMapper objectMapper = new ObjectMapper();

    @Getter
    @Setter
    private int level;

    @Getter
    @Setter
    private boolean approved;

    @Getter
    @Setter
    private boolean allMustComplete;

    @Getter
    @Setter
    private boolean allMustApprove;

    @Getter
    @Setter
    private List<String> assigneeList;

    private List<String> approvalList;
    private List<String> rejectionList;
    private AtomicInteger approvals;
    private AtomicInteger rejections;

    @JsonCreator
    public Approval(@JsonProperty("level")int level,
                    @JsonProperty("approved")boolean approved,
                    @JsonProperty("allMustComplete")boolean allMustComplete,
                    @JsonProperty("allMustApprove")boolean allMustApprove,
                    @JsonProperty("assigneeList")List<String> assigneeList,
                    @JsonProperty("approvalList")List<String> approvalList,
                    @JsonProperty("rejectionList")List<String> rejectionList,
                    @JsonProperty("approvals")AtomicInteger approvals,
                    @JsonProperty("rejections")AtomicInteger rejections){
        this.level = level;
        this.approved = approved;
        this.allMustComplete = allMustComplete;
        this.allMustApprove = allMustApprove;
        this.assigneeList = assigneeList;
        if(this.assigneeList == null){
            this.assigneeList = new ArrayList<>();
        }
        this.approvalList = approvalList;
        if(this.approvalList == null){
            this.approvalList = new ArrayList<>();
        }
        this.rejectionList = rejectionList;
        if(this.rejectionList == null){
            this.rejectionList = new ArrayList<>();
        }
        this.approvals = approvals;
        if(this.approvals == null) {
            this.approvals = new AtomicInteger();
        }
        this.rejections = rejections;
        if(this.rejections == null){
            this.rejections = new AtomicInteger();
        }
    }

    public int getApprovals() {
        return approvals.get();
    }

    public void setApprovals(AtomicInteger approvals) {
        this.approvals = approvals;
    }

    public int getRejections() {
        return rejections.get();
    }

    public void setRejections(AtomicInteger rejections) {
        this.rejections = rejections;
    }

    public List<String> getApprovalList() {
        return approvalList;
    }

    public void setApprovalList(List<String> approvalList) {
        this.approvalList = approvalList;
    }

    public List<String> getRejectionList() {
        return rejectionList;
    }

    public void setRejectionList(List<String> rejectionList) {
        this.rejectionList = rejectionList;
    }

    public int approve(String assignee) {
        this.approvalList.add(assignee);
        return this.approvals.incrementAndGet();
    }

    public int reject(String assignee) {
        this.rejectionList.add(assignee);
        return this.rejections.incrementAndGet();
    }

    @Override
    public String toString() {
        try {
            return objectMapper.writeValueAsString(this);
        } catch (JsonProcessingException jpe){
            jpe.printStackTrace();
            return "Approval{" +
                    "level='" + level + '\'' +
                    ", approved=" + approved +
                    ", allMustComplete=" + allMustComplete +
                    ", allMustApprove=" + allMustApprove +
                    ", approvalList=" + approvalList +
                    ", rejectionList=" + rejectionList +
                    ", approvals=" + approvals +
                    ", rejections=" + rejections +
                    '}';
        }
    }
}
