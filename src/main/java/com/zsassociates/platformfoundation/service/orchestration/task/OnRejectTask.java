package com.zsassociates.platformfoundation.service.orchestration.task;

import com.zsassociates.platformfoundation.service.orchestration.model.Approvals;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OnRejectTask implements JavaDelegate {

    private static final Logger logger = LoggerFactory.getLogger(OnRejectTask.class);

    public void execute(DelegateExecution execution) {
        logger.info("In Execute: OnRejectTask");
        if(null == execution)
            return;
        Approvals approvalsToTag = execution.getVariable("approvals", Approvals.class);
        approvalsToTag.setApproved(false);
        approvalsToTag.getTags().add(" :( ");
        execution.setVariable("approvals", approvalsToTag);
    }

}
