package com.zsassociates.platformfoundation.service.orchestration.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TaskRepresentation {

    private String id;
    private String name;
    private String processInstanceId;
    private Object content;
    private String assignee;
    private Map<String, Object> processVariables;
    private Map<String, Object> localVariables;
}
