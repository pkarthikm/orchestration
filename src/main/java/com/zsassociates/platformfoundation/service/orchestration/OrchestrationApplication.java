package com.zsassociates.platformfoundation.service.orchestration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OrchestrationApplication implements CommandLineRunner {

	private Logger logger = LoggerFactory.getLogger(OrchestrationApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(OrchestrationApplication.class, args);
	}

	@Override
	public void run(String... args) {
		logger.info("Starting Approval Orchestration ... ");
	}
}
