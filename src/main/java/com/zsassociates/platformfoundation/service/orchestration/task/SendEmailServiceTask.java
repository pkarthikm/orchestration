package com.zsassociates.platformfoundation.service.orchestration.task;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;

import java.util.Map;

public class SendEmailServiceTask implements JavaDelegate {

    public void execute(DelegateExecution execution) {
        //logic to sent email confirmation
        Map<String, Object> variables = execution.getVariables();
        System.out.println("In Execute: SendEmailServiceTask");
    }

}
