package com.zsassociates.platformfoundation.service.orchestration.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.Map;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class HistoricTaskRepresentation {
    private String id;
    private String name;
    private String processInstanceId;
    private Map<String, Object> processVariables;
    private Map<String, Object> localVariables;
    private String assignee;
    private Object content;
    private Date startTime;
    private Date endTime;
    private Date claimTime;
    private Long durationInMillis;
    private Long workTimeInMillis;
    private String deleteReason;
}
