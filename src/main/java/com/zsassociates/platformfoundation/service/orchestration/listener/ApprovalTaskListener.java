package com.zsassociates.platformfoundation.service.orchestration.listener;

import com.zsassociates.platformfoundation.service.orchestration.model.Approvals;
import com.zsassociates.platformfoundation.service.orchestration.model.Content;
import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ApprovalTaskListener implements TaskListener {
    private static final Logger logger = LoggerFactory.getLogger(ApprovalTaskListener.class);

    @Override
    public void notify(DelegateTask delegateTask) {
        if (null == delegateTask)
            return;
        String assignee = delegateTask.getAssignee();
        String eventName = delegateTask.getEventName();
        if(null == eventName)
            return;
        switch (eventName) {
            case "complete":
                logger.info("Task Completed: Assignee: {}", assignee);
                break;

            case "create":
                logger.info("Task Created: Assignee: {}", assignee);
                break;

            case "assignment":
                logger.info("Task Assigned: Assignee: {}", assignee);
                break;

            default:
                logger.info("Assignee: {}, eventName: {}", assignee, eventName);
                break;
        }
    }
}
