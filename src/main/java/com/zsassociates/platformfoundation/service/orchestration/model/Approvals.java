package com.zsassociates.platformfoundation.service.orchestration.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
@Setter
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY,property = "@class")
public class Approvals {
    private static final ObjectMapper objectMapper = new ObjectMapper();

    private int approvalLevels;
    private Map<Integer, Approval> approvalLevelMap;
    private boolean approved;
    private List<String> tags;

    @JsonCreator
    public Approvals(@JsonProperty("approvalLevels")int approvalLevels,
                     @JsonProperty("approvalLevelMap")Map<Integer, Approval> approvalLevelMap,
                     @JsonProperty("approved")boolean approved,
                     @JsonProperty("tags")List<String> tags){

        this.approvalLevels = approvalLevels;
        this.approvalLevelMap = approvalLevelMap;
        if(this.approvalLevelMap == null) {
            this.approvalLevelMap = new HashMap<>();
        }
        this.approved = approved;
        this.tags = tags;
        if(this.tags == null){
            this.tags = new ArrayList<>();
        }
    }

    public Approval getApproval(Integer level) {
        if(null == level || level < 0 || !this.approvalLevelMap.containsKey(level))
            return null;
        return this.approvalLevelMap.get(level);
    }

    public void setApproval(Approval approval) {
        if(null == approval)
            throw new IllegalArgumentException();
        int level = approval.getLevel();
        if(level < 0 || level >= this.approvalLevels)
            throw new IllegalArgumentException();
        this.approvalLevelMap.put(level, approval);
    }

    @Override
    public String toString() {
        try {
            return objectMapper.writeValueAsString(this);
        } catch (JsonProcessingException jpe){
            jpe.printStackTrace();
            return "Approvals{" +
                    "approvalLevels='" + approvalLevels + '\'' +
                    ", approvalLevelMap=" + approvalLevelMap +
                    ", approved=" + approved +
                    ", tags=" + tags +
                    '}';
        }
    }
}
