package com.zsassociates.platformfoundation.service.orchestration.model;

import lombok.Builder;
import lombok.Data;

import java.util.Date;
import java.util.Map;

@Data
@Builder
public class StartProcessRequest {
    private String key;
    private Object data;
    private ApprovalsRequest approvals;
}
