package com.zsassociates.platformfoundation.service.orchestration.service;

import com.zsassociates.platformfoundation.service.orchestration.model.Approval;
import com.zsassociates.platformfoundation.service.orchestration.model.Approvals;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AssigneeService {
    private static final Logger logger = LoggerFactory.getLogger(AssigneeService.class);

    public List<String> get(Approvals approvals, Integer level) {
        logger.debug("Getting assignees for level: {}", level);
        if(null == level || level < 0 || level >= approvals.getApprovalLevels())
            return null;
        Approval approval = approvals.getApproval(level);
        if(null == approval)
            return null;
        return approval.getAssigneeList();
    }
}
