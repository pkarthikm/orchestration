package com.zsassociates.platformfoundation.service.orchestration.service;

import com.zsassociates.platformfoundation.service.orchestration.model.Approval;
import com.zsassociates.platformfoundation.service.orchestration.model.Approvals;
import com.zsassociates.platformfoundation.service.orchestration.model.Content;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ApprovalService {
    private static final Logger logger = LoggerFactory.getLogger(ApprovalService.class);

    public boolean evaluate(Approvals approvals) {
        if(null == approvals)
            return false;
        int approvalLevels = approvals.getApprovalLevels();
        if(approvalLevels <= 0)
            return false;
        //Iterating over all levels
        for(int level = 0; level < approvalLevels; level++) {
            Approval approval = approvals.getApproval(level);
            //if any level is not present
            if(null == approval)
                return false;
            //if all rounds are present
            //Approval of last round will be final approval
            if(level == approvalLevels-1) {
                return approval.isApproved();
            }
        }
        return false;
    }
}
