package com.zsassociates.platformfoundation.service.orchestration.service;

import com.zsassociates.platformfoundation.service.orchestration.model.Approval;
import com.zsassociates.platformfoundation.service.orchestration.model.Approvals;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class CompletionService {
    private static final Logger logger = LoggerFactory.getLogger(CompletionService.class);

    public boolean evaluate(Approvals approvals, int nrOfCompletedInstances, int nrOfInstances, Integer level) {
        if(null == approvals)
            return false;
        int approvalLevels = approvals.getApprovalLevels();
        if(null == level || level < 0 || level >= approvalLevels)
            return false;
        Approval approval = approvals.getApproval(level);
        if(null == approval)
            return false;
        boolean allMustComplete = approval.isAllMustComplete();
        logger.info("Completed: {}. Total: {}. allMustComplete: {}",
                nrOfCompletedInstances, nrOfInstances, allMustComplete);
        if(allMustComplete)
            return (nrOfCompletedInstances == nrOfInstances);
        return (nrOfCompletedInstances > 0);
    }

    public boolean evaluateLevel(Approvals approvals, Integer level) {
        logger.info("Evaluate Level : {}", level);
        if(null == approvals)
            return false;
        int approvalLevels = approvals.getApprovalLevels();
        if(null == level || level < 0 || level >= approvalLevels)
            return false;
        Approval approval = approvals.getApproval(level);
        if(null == approval)
            return false;
        return !approval.isApproved();
    }
}
