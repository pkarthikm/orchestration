package com.zsassociates.platformfoundation.service.orchestration.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.Map;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProcessInstanceRepresentation {
    private String id;
    private String name;
    private String localizedName;
    private String localizedDescription;
    private String description;
    private String processDefinitionId;
    private String processDefinitionName;
    private String processDefinitionKey;
    private Integer processDefinitionVersion;
    private String deploymentId;
    private String businessKey;
    private Boolean isSuspended;
    private Boolean isEnded;
    private String tenantId;
    private Date startTime;
    private String startUserId;
    private Map<String, Object> processVariables;
}
