package com.zsassociates.platformfoundation.service.orchestration.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class HistoricVariableRepresentation {
    private String id;
    private String taskId;
    private String processInstanceId;
    private String name;
    private Object value;
    private String type;
    private Date createTime;
    private Date lastUpdatedTime;
}
