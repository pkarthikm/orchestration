package com.zsassociates.platformfoundation.service.orchestration.util;

import com.zsassociates.platformfoundation.service.orchestration.model.Content;

import java.util.HashSet;
import java.util.List;
import java.util.Random;

public class Utils {
    private Utils() {
    }

    public static class RandomString {

        public static Content pickRandomString() {
            String[] texts = {"hello from london", "Hi there from activiti!", "all good news over here.", "I've tweeted about activiti today.",
                    "other boring projects.", "activiti cloud - Cloud Native Java BPM"};
            return new Content(texts[new Random().nextInt(texts.length)],false);
        }
    }

    public static <T> boolean listEqualsIgnoreOrder(List<T> list1, List<T> list2) {
        return new HashSet<>(list1).equals(new HashSet<>(list2));
    }
}
