package com.zsassociates.platformfoundation.service.orchestration.listener;

import org.activiti.engine.delegate.event.ActivitiEvent;
import org.activiti.engine.delegate.event.ActivitiEventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ApprovalProcessEventListener implements ActivitiEventListener {
    private static final Logger logger = LoggerFactory.getLogger(ApprovalProcessEventListener.class);

    @Override
    public void onEvent(ActivitiEvent event) {
        logger.debug("ApprovalProcessEventListener:onEvent:type:{} processDef:{} processInst:{}",
                event.getType(), event.getProcessDefinitionId(), event.getProcessInstanceId());
        switch (event.getType()) {

            case PROCESS_STARTED:
                // ...
                break;

            case PROCESS_COMPLETED:
                // ...
                break;

            case PROCESS_CANCELLED:
                // ...
                break;

            case PROCESS_COMPLETED_WITH_ERROR_END_EVENT:
                // ...
                break;

            case ENTITY_CREATED:
                // ...
                break;

            case VARIABLE_CREATED:
                // ..
                break;

            case ENTITY_INITIALIZED:
                // ..
                break;

            case ACTIVITY_STARTED:
                // ..
                break;

            case ACTIVITY_COMPLETED:
                // ..
                break;

            case SEQUENCEFLOW_TAKEN:
                // ..
                break;

            case ENTITY_DELETED:
                // ..
                break;

            default:
                break;
        }
    }

    @Override
    public boolean isFailOnException() {
        logger.info("ApprovalProcessEventListener:isFailOnException()");
        return false;
    }
}
