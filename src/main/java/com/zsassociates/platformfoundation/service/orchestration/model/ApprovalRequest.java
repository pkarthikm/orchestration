package com.zsassociates.platformfoundation.service.orchestration.model;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

import java.util.List;

@Data
@Builder
public class ApprovalRequest {
    @NonNull
    private Boolean allMustComplete;
    @NonNull
    private Boolean allMustApprove;
    @NonNull
    private List<String> assigneeList;
}
