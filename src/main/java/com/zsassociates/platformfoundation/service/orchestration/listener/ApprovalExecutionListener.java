package com.zsassociates.platformfoundation.service.orchestration.listener;

import com.zsassociates.platformfoundation.service.orchestration.model.Content;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.activiti.engine.runtime.Execution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Map;

public class ApprovalExecutionListener implements ExecutionListener {
    private static final Logger logger = LoggerFactory.getLogger(ApprovalExecutionListener.class);
    @Override
    public void notify(DelegateExecution delegateExecution) {
        String eventName = delegateExecution.getEventName();
        Map<String, Object> vars = delegateExecution.getVariables();
        if(null == vars || StringUtils.isEmpty(eventName))
            return;
        Integer loopCounter = (Integer) vars.getOrDefault("loopCounter", null);
        Content content = (Content) vars.getOrDefault("content", null);
        logger.info("event: {}. loopCounter: {}, content: {}", eventName, loopCounter, content);
    }
}
