package com.zsassociates.platformfoundation.service.orchestration.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY,property = "@class")
public class Content {
    private static final ObjectMapper objectMapper = new ObjectMapper();

    private Object data;
    private boolean approved;


    @JsonCreator
    public Content(@JsonProperty("data")Object data,
                   @JsonProperty("approved")boolean approved){
        this.data = data;
        this.approved = approved;
    }

    @Override
    public String toString() {
        try {
            return objectMapper.writeValueAsString(this);
        } catch (JsonProcessingException jpe) {
            jpe.printStackTrace();
            return "Content{" +
                    "data='" + data + '\'' +
                    ", approved=" + approved +
                    '}';
        }
    }
}
