package com.zsassociates.platformfoundation.service.orchestration.model;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

import java.util.Map;

@Data
@Builder
public class ApprovalsRequest {
    @NonNull
    private Integer approvalLevels;
    @NonNull
    private Map<Integer, ApprovalRequest> approvalLevelMap;
}
