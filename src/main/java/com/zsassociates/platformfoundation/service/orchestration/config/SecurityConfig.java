package com.zsassociates.platformfoundation.service.orchestration.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
                //.authorizeRequests()
                //.antMatchers("/v2/api-docs",
                //        "/configuration/**",
                //        "/swagger*/**",
                //        "/webjars/**")
                //.permitAll()
                //.anyRequest().authenticated();

        http.headers().frameOptions().disable();

        http.cors().disable();
                /*.and()
                .authorizeRequests()
                .antMatchers(HttpMethod.GET, "/orchestration/approval/*")
                .hasAuthority("SCOPE_read")
                .antMatchers(HttpMethod.POST, "/orchestration/approval/*")
                .hasAuthority("SCOPE_write")
                .antMatchers(HttpMethod.DELETE, "/orchestration/approval/*")
                .hasAuthority("SCOPE_write")
                .anyRequest()
                .authenticated()
                .and()
                .oauth2ResourceServer()
                .jwt();*/
    }
}

