package com.zsassociates.platformfoundation.service.orchestration.service;

import com.zsassociates.platformfoundation.service.orchestration.model.*;
import com.zsassociates.platformfoundation.service.orchestration.util.Utils;
import org.activiti.engine.ActivitiException;
import org.activiti.engine.HistoryService;
import org.activiti.engine.TaskService;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.history.HistoricTaskInstanceQuery;
import org.activiti.engine.history.HistoricVariableInstance;
import org.activiti.engine.task.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class TaskUpdateService {
    private static final Logger logger = LoggerFactory.getLogger(TaskUpdateService.class);

    @Autowired
    TaskService taskService;

    @Autowired
    private HistoryService historyService;

    public List<TaskRepresentation> listTasks() {
        List<Task> tasks = taskService.createTaskQuery()
                .includeProcessVariables()
                .includeTaskLocalVariables()
                .orderByTaskCreateTime().asc().list();

        if(null == tasks || tasks.isEmpty())
            return new ArrayList<TaskRepresentation>();

        return tasks.stream()
                .map(task -> TaskRepresentation.builder()
                        .id(task.getId())
                        .name(task.getName())
                        .processInstanceId(task.getProcessInstanceId())
                        .processVariables(task.getProcessVariables())
                        .localVariables(task.getTaskLocalVariables())
                        .assignee(task.getAssignee())
                        .build())
                .collect(Collectors.toList());
    }

    public List<TaskRepresentation> getTasks(String processInstanceId) throws IllegalArgumentException {
        if(StringUtils.isEmpty(processInstanceId))
            throw new IllegalArgumentException("Process Instance Id cannot be null");

        List<Task> tasks = taskService.createTaskQuery()
                .processInstanceId(processInstanceId)
                .includeProcessVariables()
                .includeTaskLocalVariables()
                .list();

        if(null == tasks || tasks.isEmpty())
            return new ArrayList<TaskRepresentation>();

        return tasks.stream()
                .map(task -> TaskRepresentation.builder()
                        .id(task.getId())
                        .name(task.getName())
                        .processInstanceId(task.getProcessInstanceId())
                        .processVariables(task.getProcessVariables())
                        .localVariables(task.getTaskLocalVariables())
                        .assignee(task.getAssignee())
                        .build())
                .collect(Collectors.toList());
    }

    public List<TaskRepresentation> getTasksByAssignee(String assignee) throws IllegalArgumentException {
        if(StringUtils.isEmpty(assignee))
            throw new IllegalArgumentException("Assignee cannot be null");

        List<Task> tasks = taskService.createTaskQuery()
                .taskAssignee(assignee)
                .includeProcessVariables()
                .includeTaskLocalVariables()
                .list();

        if(null == tasks || tasks.isEmpty())
            return new ArrayList<TaskRepresentation>();

        return tasks.stream()
                .map(task -> TaskRepresentation.builder()
                        .id(task.getId())
                        .name(task.getName())
                        .processInstanceId(task.getProcessInstanceId())
                        .processVariables(task.getProcessVariables())
                        .localVariables(task.getTaskLocalVariables())
                        .assignee(task.getAssignee())
                        .build())
                .collect(Collectors.toList());
    }

    public void reviewTask(Task task, Boolean isApproved) throws IllegalArgumentException {
        //Validate the task Id
        if(null == task || null == isApproved)
            throw new IllegalArgumentException();

        String taskId = task.getId();

        logger.info("isApproved: {}", isApproved);
        Map<String, Object> variablesMap = new HashMap<>();
        Content contentToProcess = taskService.getVariable(taskId, "content", Content.class);
        if(null == contentToProcess) {
            logger.info("No content received inside the task for approval. Completing without variables... ");
            taskService.complete(taskId);
            return;
        }
        logger.info("> Content received inside the task to approve: " + contentToProcess);

        Integer level = taskService.getVariable(taskId, "levelCounter", Integer.class);
        String assignee = task.getAssignee();
        Map<String, Object> procVars = task.getProcessVariables();
        Approvals approvals = (Approvals) procVars.get("approvals");
        if(null != approvals && !StringUtils.isEmpty(assignee) && null != level) {
            Approval approval = approvals.getApproval(level);
            if(null != approval) {
                if (isApproved) {
                    logger.info("> User Approving content");
                    approval.approve(assignee);
                } else {
                    logger.info("> User Rejecting content");
                    approval.reject(assignee);
                }
                approval.setApproved(isLevelApproved(approval, level));
                variablesMap.put("approvals", approvals);
                taskService.setVariable(taskId, "approvals", approvals);
            }
        }
        contentToProcess.setApproved(isApproved);
        logger.info("> processed content: " + contentToProcess);
        variablesMap.put("content", contentToProcess);
        taskService.setVariable(taskId,"content", contentToProcess);
        taskService.complete(taskId, variablesMap);
        logger.info("Task completed: {}. Reviewed Content: {}", taskId, contentToProcess);
    }

    public Task getTask(String taskId) throws IllegalArgumentException {
        if(StringUtils.isEmpty(taskId))
            throw new IllegalArgumentException();

        return taskService.createTaskQuery()
                .active()
                .taskId(taskId)
                .includeProcessVariables()
                .singleResult();
    }

    public List<HistoricTaskRepresentation> getHistoricTasks(String processInstanceId)
            throws IllegalArgumentException {
        if(StringUtils.isEmpty(processInstanceId))
            throw new IllegalArgumentException("Process Instance Id cannot be null");
        HistoricTaskInstanceQuery hTaskInstQ = historyService.createHistoricTaskInstanceQuery()
                .includeProcessVariables()
                .includeTaskLocalVariables()
                .processInstanceId(processInstanceId);
        if(null == hTaskInstQ)
            return null;
        List<HistoricTaskInstance> hTaskInstances = hTaskInstQ.list();
        if(null == hTaskInstances || hTaskInstances.isEmpty())
            return null;

        return hTaskInstances.stream()
                .map(hTaskInst -> HistoricTaskRepresentation.builder()
                        .id(hTaskInst.getId())
                        .name(hTaskInst.getName())
                        .processInstanceId(hTaskInst.getProcessInstanceId())
                        .processVariables(hTaskInst.getProcessVariables())
                        .localVariables(hTaskInst.getTaskLocalVariables())
                        .assignee(hTaskInst.getAssignee())
                        .content(historyService.createHistoricVariableInstanceQuery()
                                .taskId(hTaskInst.getId())
                                .variableName("content")
                                .singleResult()
                                .getValue())
                        .startTime(hTaskInst.getStartTime())
                        .endTime(hTaskInst.getEndTime())
                        .durationInMillis(hTaskInst.getDurationInMillis())
                        .workTimeInMillis(hTaskInst.getWorkTimeInMillis())
                        .deleteReason(hTaskInst.getDeleteReason())
                        .claimTime(hTaskInst.getClaimTime())
                        .build())
                .collect(Collectors.toList());
    }

    public List<HistoricTaskRepresentation> getHistoricTasksByAssignee(String assignee)
            throws IllegalArgumentException {
        if(StringUtils.isEmpty(assignee))
            throw new IllegalArgumentException("Assignee cannot be null");
        HistoricTaskInstanceQuery hTaskInstQ = historyService.createHistoricTaskInstanceQuery()
                .includeProcessVariables()
                .includeTaskLocalVariables()
                .taskAssignee(assignee)
                .finished()
                .orderByHistoricTaskInstanceEndTime()
                .desc();
        if(null == hTaskInstQ)
            return null;
        //Limiting to last 20 results only
        List<HistoricTaskInstance> hTaskInstances = hTaskInstQ.listPage(0,5);
        if(null == hTaskInstances || hTaskInstances.isEmpty())
            return null;

        return hTaskInstances.stream()
                .map(hTaskInst -> HistoricTaskRepresentation.builder()
                        .id(hTaskInst.getId())
                        .name(hTaskInst.getName())
                        .processInstanceId(hTaskInst.getProcessInstanceId())
                        .processVariables(hTaskInst.getProcessVariables())
                        .localVariables(hTaskInst.getTaskLocalVariables())
                        .assignee(hTaskInst.getAssignee())
                        .content(historyService.createHistoricVariableInstanceQuery()
                                .taskId(hTaskInst.getId())
                                .variableName("content")
                                .singleResult()
                                .getValue())
                        .startTime(hTaskInst.getStartTime())
                        .endTime(hTaskInst.getEndTime())
                        .durationInMillis(hTaskInst.getDurationInMillis())
                        .workTimeInMillis(hTaskInst.getWorkTimeInMillis())
                        .deleteReason(hTaskInst.getDeleteReason())
                        .claimTime(hTaskInst.getClaimTime())
                        .build())
                .collect(Collectors.toList());
    }

    public List<HistoricVariableRepresentation> getHistoricVariables(String processInstanceId, Boolean excludeTaskVariables)
            throws IllegalArgumentException {
        if(StringUtils.isEmpty(processInstanceId) || null == excludeTaskVariables)
            throw new IllegalArgumentException("Process Instance Id cannot be null");
        List<HistoricVariableInstance> hVarInstances = null;
        if(excludeTaskVariables) {
            hVarInstances = historyService.createHistoricVariableInstanceQuery()
                    .excludeTaskVariables()
                    .processInstanceId(processInstanceId)
                    .list();
        } else {
            hVarInstances = historyService.createHistoricVariableInstanceQuery()
                    .processInstanceId(processInstanceId)
                    .list();
        }

        if(null == hVarInstances || hVarInstances.isEmpty())
            return new ArrayList<HistoricVariableRepresentation>();

        return hVarInstances.stream()
                .map(hVarInst -> HistoricVariableRepresentation.builder()
                        .id(hVarInst.getId())
                        .taskId(hVarInst.getTaskId())
                        .processInstanceId(hVarInst.getProcessInstanceId())
                        .name(hVarInst.getVariableName())
                        .value(hVarInst.getValue())
                        .type(hVarInst.getVariableTypeName())
                        .createTime(hVarInst.getCreateTime())
                        .lastUpdatedTime(hVarInst.getLastUpdatedTime())
                        .build())
                .collect(Collectors.toList());
    }

    public HistoricTaskRepresentation getHistoricTask(String taskId) {
        if(StringUtils.isEmpty(taskId))
            throw new IllegalArgumentException("Task Id cannot be null");
        HistoricTaskInstance hTaskInst = null;
        try {
            hTaskInst = historyService.createHistoricTaskInstanceQuery()
                    .includeProcessVariables()
                    .includeTaskLocalVariables()
                    .taskId(taskId)
                    .singleResult();
        } catch(ActivitiException ae) {
            return null;
        }

        if(null == hTaskInst)
            return null;

        return HistoricTaskRepresentation.builder()
                .id(hTaskInst.getId())
                .name(hTaskInst.getName())
                .processInstanceId(hTaskInst.getProcessInstanceId())
                .processVariables(hTaskInst.getProcessVariables())
                .localVariables(hTaskInst.getTaskLocalVariables())
                .assignee(hTaskInst.getAssignee())
                .content(historyService.createHistoricVariableInstanceQuery()
                        .taskId(hTaskInst.getId())
                        .variableName("content")
                        .singleResult()
                        .getValue())
                .startTime(hTaskInst.getStartTime())
                .endTime(hTaskInst.getEndTime())
                .durationInMillis(hTaskInst.getDurationInMillis())
                .workTimeInMillis(hTaskInst.getWorkTimeInMillis())
                .deleteReason(hTaskInst.getDeleteReason())
                .claimTime(hTaskInst.getClaimTime())
                .build();
    }

    private boolean isLevelApproved(Approval approval, Integer level) {
        if(null == approval || null == level)
            return false;

        List<String> assigneeList = approval.getAssigneeList();
        if(null == assigneeList || assigneeList.isEmpty())
            return false;

        int assigneeCount = assigneeList.size();

        int approvalCount = approval.getApprovals();
        int rejectionCount = approval.getRejections();
        if(approvalCount < 0 || rejectionCount < 0)
            return false;
        List<String> approvalList = approval.getApprovalList();
        if(null == approvalList)
            return false;
        List<String> rejectionList = approval.getRejectionList();
        if(null == rejectionList)
            return false;
        boolean allMustComplete = approval.isAllMustComplete();
        boolean allMustApprove = approval.isAllMustApprove();
        if(allMustComplete){
            //All have completed
            if(assigneeCount == approvalCount + rejectionCount &&
                    Utils.listEqualsIgnoreOrder(assigneeList, Stream.of(approvalList, rejectionList)
                            .flatMap(Collection::stream)
                            .collect(Collectors.toList()))) {
                if(allMustApprove) {
                    //No Rejections
                    return (rejectionCount <= 0 && rejectionList.isEmpty());
                } else {
                    //Some Approvals
                    return (approvalCount > 0 && !approvalList.isEmpty());
                }
            } else {
                //All have not completed yet
                return false;
            }
        } else {
            //More than one completed
            if(approvalCount > 0 ||
                    rejectionCount > 0 ||
                    !approvalList.isEmpty() ||
                    !rejectionList.isEmpty()) {
                if(allMustApprove) {
                    //All Completed should have approved - No rejections
                    return (rejectionCount <= 0 && rejectionList.isEmpty());
                } else {
                    //Come must have approved
                    return (approvalCount > 0 && !approvalList.isEmpty());
                }
            } else {
                //None have completed
                return false;
            }
        }
    }
}
