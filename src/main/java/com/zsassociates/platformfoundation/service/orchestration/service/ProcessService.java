package com.zsassociates.platformfoundation.service.orchestration.service;

import com.zsassociates.platformfoundation.service.orchestration.model.*;
import org.activiti.engine.ActivitiException;
import org.activiti.engine.ActivitiObjectNotFoundException;
import org.activiti.engine.HistoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.history.HistoricProcessInstanceQuery;
import org.activiti.engine.runtime.ProcessInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ProcessService {
    private static final Logger logger = LoggerFactory.getLogger(ProcessService.class);

    @Autowired
    private RuntimeService runtimeService;

    @Autowired
    private CompletionService completionService;

    @Autowired
    private ApprovalService approvalService;

    @Autowired
    private AssigneeService assigneeService;

    @Autowired
    private HistoryService historyService;

    public ProcessInstanceRepresentation startProcess(StartProcessRequest startProcessRequest)
            throws IllegalArgumentException, ActivitiObjectNotFoundException {
        if( null == startProcessRequest)
            throw new IllegalArgumentException("This method takes non null arguments");
        String key = startProcessRequest.getKey();
        if(StringUtils.isEmpty(key))
            throw new IllegalArgumentException("Key cannot be null");
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yy HH:mm:ss");
        Object data = startProcessRequest.getData();
        if(null == data)
            throw new IllegalArgumentException("Text to be reviewed cannot be empty");
        Content content = new Content(data,false);
        Map<String, Object> variablesMap = new HashMap<>();
        variablesMap.put("content", content);

        ApprovalsRequest approvalsRequest = startProcessRequest.getApprovals();
        if(null == approvalsRequest)
            throw new IllegalArgumentException("Approvals cannot be null");
        Integer approvalLevels = approvalsRequest.getApprovalLevels();
        if(null == approvalLevels || approvalLevels <= 0 )
            throw new IllegalArgumentException("Approval levels have to be greater than ZERO");
        Map<Integer, ApprovalRequest> approvalLevelMapRequest = approvalsRequest.getApprovalLevelMap();
        if(null == approvalLevelMapRequest || approvalLevelMapRequest.isEmpty())
            throw new IllegalArgumentException("Approvals cannot be empty");
        variablesMap.put("approvalLevels", approvalLevels);

        Map<Integer, Approval> approvalLevelMap = new HashMap<>();
        //Iterating as well as validating for gaps in the Map
        for(int level = 0; level < approvalLevels; level++) {
            if(!approvalLevelMapRequest.containsKey(level))
                throw new IllegalArgumentException("Map does not contain information for level: " + level);
            ApprovalRequest approvalRequest = approvalLevelMapRequest.get(level);
            Approval approval = new Approval(
                    level,
                    false,
                    approvalRequest.getAllMustComplete(),
                    approvalRequest.getAllMustApprove(),
                    approvalRequest.getAssigneeList(),
                    null, null, null, null);
            approvalLevelMap.put(level, approval);
        }
        Approvals approvals = new Approvals(approvalLevels, approvalLevelMap, false, null);
        variablesMap.put("approvals", approvals);
        //Not user input as of now
        variablesMap.put("completionService", completionService);
        variablesMap.put("approvalService", approvalService);
        variablesMap.put("assigneeService", assigneeService);

        logger.info("> Starting process content: {} approvals: {} approvalLevels: {} at {} ",
                content,
                approvals,
                approvalLevels,
                formatter.format(new Date()));
        ProcessInstance pi = runtimeService.startProcessInstanceByKey(key, variablesMap);
        ProcessInstanceRepresentation processInstRep = buildProcessInstRepFromProcessInstance(pi);
        processInstRep.setProcessVariables(variablesMap);
        return processInstRep;
    }

    public List<ProcessInstanceRepresentation> listProcessInstances(Boolean isActive)
            throws ActivitiException {
        if(null == isActive)
            isActive = false;
        List<ProcessInstance> processInstances = null;
        if(isActive)
            processInstances = runtimeService.createProcessInstanceQuery()
                    .includeProcessVariables()
                    .active()
                    .list();
        else
            processInstances = runtimeService.createProcessInstanceQuery()
                    .includeProcessVariables()
                    .list();

        if(null == processInstances || processInstances.isEmpty())
            return new ArrayList<ProcessInstanceRepresentation>();

        return processInstances.stream()
                .map(this::buildProcessInstRepFromProcessInstance)
                .collect(Collectors.toList());
    }

    public ProcessInstanceRepresentation getProcess(String processInstanceId)
            throws IllegalArgumentException, ActivitiException {
        if(StringUtils.isEmpty(processInstanceId))
            throw new IllegalArgumentException("Process Instance Id cannot be null");

        ProcessInstance processInstance = runtimeService.createProcessInstanceQuery()
                .includeProcessVariables()
                .processInstanceId(processInstanceId)
                .singleResult();

        if(null == processInstance)
            return null;

        return buildProcessInstRepFromProcessInstance(processInstance);
    }

    public HistoricProcessRepresentation getHistoricProcess(String processInstanceId)
            throws IllegalArgumentException {
        if(StringUtils.isEmpty(processInstanceId))
            throw new IllegalArgumentException("Process Instance Id cannot be null");
        HistoricProcessInstanceQuery hProcessInstQ = historyService.createHistoricProcessInstanceQuery()
                .includeProcessVariables()
                .processInstanceId(processInstanceId);
        if(null == hProcessInstQ)
            return null;
        List<HistoricProcessInstance> hProcessInstances = hProcessInstQ.list();
        if(null == hProcessInstances || hProcessInstances.isEmpty())
            return null;

        HistoricProcessInstance hProcInst = hProcessInstances.get(0); //Should be only one with the given processInstanceId
        if(null == hProcInst)
            return null;
        return HistoricProcessRepresentation.builder()
                .id(hProcInst.getId())
                .name(hProcInst.getName())
                .businessKey(hProcInst.getBusinessKey())
                .deleteReason(hProcInst.getDeleteReason())
                .deploymentId(hProcInst.getDeploymentId())
                .description(hProcInst.getDescription())
                .durationInMillis(hProcInst.getDurationInMillis())
                .endActivityId(hProcInst.getEndActivityId())
                .endTime(hProcInst.getEndTime())
                .processDefinitionId(hProcInst.getProcessDefinitionId())
                .processDefinitionKey(hProcInst.getProcessDefinitionKey())
                .processDefinitionName(hProcInst.getProcessDefinitionName())
                .processDefinitionVersion(hProcInst.getProcessDefinitionVersion())
                .processVariables(hProcInst.getProcessVariables())
                .startActivityId(hProcInst.getStartActivityId())
                .startTime(hProcInst.getStartTime())
                .startUserId(hProcInst.getStartUserId())
                .superProcessInstanceId(hProcInst.getSuperProcessInstanceId())
                .tenantId(hProcInst.getTenantId())
                .build();

    }

    public Map<String, Object> getHistoricProcessVariables(String processInstanceId)
            throws IllegalArgumentException {
        if(StringUtils.isEmpty(processInstanceId))
            throw new IllegalArgumentException("Process Instance Id cannot be null");
        HistoricProcessInstanceQuery hProcessInstQ = historyService.createHistoricProcessInstanceQuery()
                .includeProcessVariables()
                .processInstanceId(processInstanceId);
        if(null == hProcessInstQ)
            return null;
        List<HistoricProcessInstance> hProcessInstances = hProcessInstQ.list();
        if(null == hProcessInstances || hProcessInstances.isEmpty())
            return null;
        HistoricProcessInstance hProcInst = hProcessInstances.get(0);
        if(null == hProcInst)
            return null;
        return hProcInst.getProcessVariables();
    }

    public void deleteProcess(String processInstanceId)
            throws IllegalArgumentException,ActivitiObjectNotFoundException  {
        if(StringUtils.isEmpty(processInstanceId))
            throw new IllegalArgumentException();

        runtimeService.deleteProcessInstance(processInstanceId, "User Terminated");
    }

    public List<String> deleteAllProcessInstances() {
        List<ProcessInstance> processInstances = runtimeService.createProcessInstanceQuery()
                .list();

        if(null == processInstances || processInstances.isEmpty())
            return null;

        List<String> notDeletedProcessInstances = new ArrayList<>();
        for (ProcessInstance processInstance: processInstances) {
            String pid = processInstance.getId();
            try {
                runtimeService.deleteProcessInstance(pid, "User Terminated");
            } catch(ActivitiObjectNotFoundException aonfe) {
                logger.debug("Not found the process instance to delete:{}", pid);
            } catch (Exception ex) {
                notDeletedProcessInstances.add(pid);
            }
        }

        return notDeletedProcessInstances;
    }

    private ProcessInstanceRepresentation buildProcessInstRepFromProcessInstance(ProcessInstance processInstance) {
        if(null == processInstance)
            return null;
        return ProcessInstanceRepresentation.builder()
                .id(processInstance.getId())
                .name(processInstance.getName())
                .description(processInstance.getDescription())
                .processVariables(processInstance.getProcessVariables())
                .businessKey(processInstance.getBusinessKey())
                .deploymentId(processInstance.getDeploymentId())
                .processDefinitionKey(processInstance.getProcessDefinitionKey())
                .localizedDescription(processInstance.getLocalizedDescription())
                .localizedName(processInstance.getLocalizedName())
                .processDefinitionId(processInstance.getProcessDefinitionId())
                .processDefinitionName(processInstance.getProcessDefinitionName())
                .processDefinitionVersion(processInstance.getProcessDefinitionVersion())
                .startTime(processInstance.getStartTime())
                .startUserId(processInstance.getStartUserId())
                .tenantId(processInstance.getTenantId())
                .isSuspended(processInstance.isSuspended())
                .isEnded(processInstance.isEnded())
                .build();
    }
}
