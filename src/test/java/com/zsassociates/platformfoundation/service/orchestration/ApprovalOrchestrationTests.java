package com.zsassociates.platformfoundation.service.orchestration;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zsassociates.platformfoundation.service.orchestration.model.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.annotation.Order;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.*;

@RunWith(SpringRunner.class)
@SpringBootTest(
		webEnvironment = SpringBootTest.WebEnvironment.MOCK,
		classes = OrchestrationApplication.class)
@AutoConfigureMockMvc
/*@TestPropertySource(
		locations = "classpath:approval-orchestration.yml")*/
public class ApprovalOrchestrationTests {

	private static final ObjectMapper objectMapper = new ObjectMapper();

	private static final Random random = new Random();

	@Autowired
	private MockMvc mvc;

	@Test
	@Order(1)
	public void initCleanUp() throws Exception {
		mvc.perform(delete("/approval/delete")
				.contentType(MediaType.APPLICATION_JSON_UTF8))
				.andDo(print())
				.andExpect(status().isOk());
	}

	@Test
	@Order(2)
	public void noProcessesAtStart() throws Exception {
		//All active
		List<ProcessInstanceRepresentation> activePInstReps = listProcesses(true);
		assertNotNull(activePInstReps);
		assertTrue(activePInstReps.isEmpty());
		//All Processes
		List<ProcessInstanceRepresentation> pInstReps = listProcesses(false);
		assertNotNull(pInstReps);
		assertTrue(pInstReps.isEmpty());
	}

	@Test
	@Order(3)
	public void multiLevelRejected_WithOneRejection() throws Exception {
		//Start a process
		ProcessInstanceRepresentation pInstRep = startApprovalProcess(2, false, false);
		assertNotNull(pInstRep);
		String processInstanceId = pInstRep.getId();

		//List to see tasks have started for the process
		List<TaskRepresentation> tReps = listTasks();
		assertNotNull(tReps);
		assertFalse(tReps.isEmpty());
		assertEquals(tReps.size(), 3);

		//List and Reject any Tasks randomly
		String taskAssignee = listAndReviewRandomTask(false);

		//List Tasks to verify if all tasks ended
		List<TaskRepresentation> tRepsAfterReject = listTasks();
		assertNotNull(tRepsAfterReject);
		assertTrue(tRepsAfterReject.isEmpty());

		//Verify Process has ended properly
		List<ProcessInstanceRepresentation> pInstReps = listProcesses(false);
		assertNotNull(pInstReps);
		assertTrue(pInstReps.isEmpty());

		//Get Historic Process Variables and check if process is finally rejected with :( tag
		checkApprovalFor(processInstanceId, false);
	}

	@Test
	@Order(4)
	public void multiLevelApproved_OnlyAfterAllApprovals() throws Exception {
		//Start a process
		ProcessInstanceRepresentation pInstRep = startApprovalProcess(2, true, true);
		assertNotNull(pInstRep);
		String processInstanceId = pInstRep.getId();

		//Keep Approving till all tasks are done - this would be 6 tasks = 3*2
		//Level 0
		listAndReviewRandomTask(true);
		listAndReviewRandomTask(true);
		listAndReviewRandomTask(true);
		//Level 1
		listAndReviewRandomTask(true);
		listAndReviewRandomTask(true);
		listAndReviewRandomTask(true);

		//List Tasks to verify if all tasks ended
		List<TaskRepresentation> tRepsAfterReject = listTasks();
		assertNotNull(tRepsAfterReject);
		assertTrue(tRepsAfterReject.isEmpty());

		//Verify Process has ended properly
		List<ProcessInstanceRepresentation> pInstReps = listProcesses(false);
		assertNotNull(pInstReps);
		assertTrue(pInstReps.isEmpty());

		//Get Historic Process Variables and check if process is finally approved with :) tag
		checkApprovalFor(processInstanceId, true);
	}

	@Test
	@Order(5)
	public void multiLevelApproved_OnlyAfterAllCompletionButOneApprovalPerLevel() throws Exception {
		//Start a process
		ProcessInstanceRepresentation pInstRep = startApprovalProcess(2, true, false);
		assertNotNull(pInstRep);
		String processInstanceId = pInstRep.getId();

		//Keep Approving till all tasks are done - this would be 6 tasks = 3*2
		//Level 0
		listAndReviewRandomTask(false);
		listAndReviewRandomTask(false);
		listAndReviewRandomTask(true);
		//Level 1
		listAndReviewRandomTask(false);
		listAndReviewRandomTask(true);
		listAndReviewRandomTask(false);

		//List Tasks to verify if all tasks ended
		List<TaskRepresentation> tRepsAfterReject = listTasks();
		assertNotNull(tRepsAfterReject);
		assertTrue(tRepsAfterReject.isEmpty());

		//Verify Process has ended properly
		List<ProcessInstanceRepresentation> pInstReps = listProcesses(false);
		assertNotNull(pInstReps);
		assertTrue(pInstReps.isEmpty());

		//Get Historic Process Variables and check if process is finally approved with :) tag
		checkApprovalFor(processInstanceId, true);
	}

	@Test
	@Order(6)
	public void multiLevelApproved_EvenWithOneApprovalAtEachLevel() throws Exception {
		//Start a process
		ProcessInstanceRepresentation pInstRep = startApprovalProcess(2, false, true);
		assertNotNull(pInstRep);
		String processInstanceId = pInstRep.getId();

		//Keep Approving till all tasks are done - this would be 6 tasks = 3*2
		//Level 0
		listAndReviewRandomTask(true);

		//Level 1
		listAndReviewRandomTask(true);

		//List Tasks to verify if all tasks ended
		List<TaskRepresentation> tRepsAfterReject = listTasks();
		assertNotNull(tRepsAfterReject);
		assertTrue(tRepsAfterReject.isEmpty());

		//Verify Process has ended properly
		List<ProcessInstanceRepresentation> pInstReps = listProcesses(false);
		assertNotNull(pInstReps);
		assertTrue(pInstReps.isEmpty());

		//Get Historic Process Variables and check if process is finally approved with :) tag
		checkApprovalFor(processInstanceId, true);
	}

	/*@Test
	@Order(7)
	public void getHistoricTasksByAssignee() throws Exception {
		List<HistoricTaskRepresentation> histTasks = getHistoricTasksByAssignee("a0");
		assertNotNull(histTasks);
		assertTrue(histTasks.size() <= 5);
	}*/

	private List<ProcessInstanceRepresentation> listProcesses(Boolean isActive) throws Exception {
		MvcResult mvcResult = mvc.perform(get("/approval/list-processes")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.param("isActive", isActive.toString()))
				.andDo(print())
				.andExpect(status().isOk())
				.andReturn();
		MockHttpServletResponse mHSResp = mvcResult.getResponse();
		String jsonResp = mHSResp.getContentAsString();
		assertFalse(jsonResp.isEmpty());
		return Arrays.asList(objectMapper.readValue(jsonResp,
				ProcessInstanceRepresentation[].class));
	}

	private List<TaskRepresentation> listTasks() throws Exception {
		MvcResult mvcResult = mvc.perform(get("/approval/list-tasks")
				.contentType(MediaType.APPLICATION_JSON_UTF8))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_UTF8))
				.andReturn();
		MockHttpServletResponse mHSResp = mvcResult.getResponse();
		String jsonResp = mHSResp.getContentAsString();
		assertFalse(jsonResp.isEmpty());
		return Arrays.asList(objectMapper.readValue(jsonResp,
				TaskRepresentation[].class));
	}

	private List<TaskRepresentation> getTasksByAssignee(String assignee) throws Exception {
		MvcResult mvcResult = mvc.perform(get("/approval/get-tasks-by-assignee/" + assignee)
				.contentType(MediaType.APPLICATION_JSON_UTF8))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_UTF8))
				.andReturn();
		MockHttpServletResponse mHSResp = mvcResult.getResponse();
		String jsonResp = mHSResp.getContentAsString();
		assertFalse(jsonResp.isEmpty());
		return Arrays.asList(objectMapper.readValue(jsonResp,
				TaskRepresentation[].class));
	}

	private List<HistoricTaskRepresentation> getHistoricTasksByAssignee(String assignee) throws Exception {
		MvcResult mvcResult = mvc.perform(get("/approval/get-historic-tasks-by-assignee/" + assignee)
				.contentType(MediaType.APPLICATION_JSON_UTF8))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_UTF8))
				.andReturn();
		MockHttpServletResponse mHSResp = mvcResult.getResponse();
		String jsonResp = mHSResp.getContentAsString();
		assertFalse(jsonResp.isEmpty());
		return Arrays.asList(objectMapper.readValue(jsonResp,
				HistoricTaskRepresentation[].class));
	}

	private void reviewTask(String taskId, Boolean isApproved) throws Exception {
		assertNotNull(isApproved);
		mvc.perform(get("/approval/review-task/" + taskId)
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.param("isApproved", isApproved.toString()))
				.andDo(print())
				.andExpect(status().isOk());
	}

	private String listAndReviewRandomTask(Boolean isApproved) throws Exception {
		assertNotNull(isApproved);
		List<TaskRepresentation> tReps = listTasks();
		assertNotNull(tReps);
		assertFalse(tReps.isEmpty());

		TaskRepresentation anyTask = tReps.get(random.nextInt(tReps.size()));
		assertNotNull(anyTask);
		String taskId = anyTask.getId();
		assertFalse(taskId.isEmpty());
		String taskAssignee = anyTask.getAssignee();
		List<TaskRepresentation> tAssigneeReps = getTasksByAssignee(taskAssignee);
		assertNotNull(tAssigneeReps);
		assertFalse(tAssigneeReps.isEmpty());
		assertEquals(tAssigneeReps.size(), 1);
		reviewTask(taskId,isApproved);
		return taskAssignee;
	}

	private ProcessInstanceRepresentation startApprovalProcess(Integer approvalLevels, Boolean allMustComplete, Boolean allMustApprove)
			throws Exception {
		Map<Integer, ApprovalRequest> approvalLevelMap = new HashMap<>();
		for(Integer level = 0; level < approvalLevels; level++) {
			approvalLevelMap.put(level, ApprovalRequest.builder()
					.allMustApprove(allMustApprove)
					.allMustComplete(allMustComplete)
					.assigneeList(List.of(
							"a" + level.toString(),
							"b" + level.toString(),
							"c" + level.toString()))
					.build());
		}
		ApprovalsRequest approvalsRequest = ApprovalsRequest.builder()
				.approvalLevels(approvalLevels)
				.approvalLevelMap(approvalLevelMap)
				.build();
		String jsonInString = "{\"team\":\"Oncology\",\"changeCount\":20,\"before\":{},\"after\":{}}";
		Object data = objectMapper.readValue(jsonInString,Object.class);
		StartProcessRequest startProcessRequest = StartProcessRequest.builder()
				.key("humanTextApprovalProcess")
				.data(data)
				.approvals(approvalsRequest)
				.build();
		MvcResult mvcPostResult = mvc.perform(post("/approval/start-process")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(objectMapper.writeValueAsString(startProcessRequest)))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_UTF8))
				.andReturn();
		MockHttpServletResponse mHSPostResp = mvcPostResult.getResponse();
		String jsonPostResp = mHSPostResp.getContentAsString();
		assertFalse(jsonPostResp.isEmpty());
		ProcessInstanceRepresentation pInstRep = objectMapper.readValue(jsonPostResp,
				ProcessInstanceRepresentation.class);
		assertNotNull(pInstRep);
		assertEquals(pInstRep.getProcessDefinitionKey(), "humanTextApprovalProcess");
		return pInstRep;
	}

	private List<HistoricVariableRepresentation> getHistoricVariables(String processInstanceId, Boolean includeTaskVariables)
			throws Exception{
		assertFalse(processInstanceId.isEmpty());
		assertNotNull(includeTaskVariables);
		MvcResult mvcResult = mvc.perform(get("/approval/get-historic-variables/" + processInstanceId)
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.param("includeTaskVariables", includeTaskVariables.toString()))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_UTF8))
				.andReturn();
		MockHttpServletResponse mHSResp = mvcResult.getResponse();
		String jsonResp = mHSResp.getContentAsString();
		assertFalse(jsonResp.isEmpty());
		return Arrays.asList(objectMapper.readValue(jsonResp,
				HistoricVariableRepresentation[].class));
	}

	private Map<String, Object> getHistoricProcessVariables(String processInstanceId)
			throws Exception {
		assertFalse(processInstanceId.isEmpty());
		MvcResult mvcResult = mvc.perform(get("/approval/get-historic-process-variables/" + processInstanceId)
				.contentType(MediaType.APPLICATION_JSON_UTF8))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_UTF8))
				.andReturn();
		MockHttpServletResponse mHSResp = mvcResult.getResponse();
		String jsonResp = mHSResp.getContentAsString();
		assertFalse(jsonResp.isEmpty());
		return objectMapper.readValue(jsonResp, Map.class);
	}

	private void checkApprovalFor(String processInstanceId, Boolean isApproved)
			throws Exception {
		assertFalse(processInstanceId.isEmpty());
		assertNotNull(isApproved);
		Map<String, Object> procVariables = getHistoricProcessVariables(processInstanceId);
		assertNotNull(procVariables);
		assertFalse(procVariables.isEmpty());
		Map<String, Object> approvals = (LinkedHashMap) procVariables.get("approvals");
		assertNotNull(approvals);
		boolean procApproved = (boolean)approvals.get("approved");
		List<String> tags = (List<String>) approvals.get("tags");
		assertNotNull(tags);
		assertFalse(tags.isEmpty());
		if(!isApproved) {
			assertFalse(procApproved);
			assertTrue(tags.get(0).contains(":(")); //Rejected
		} else {
			assertTrue(procApproved);
			assertTrue(tags.get(0).contains(":)")); //Approved
		}
	}

	@Test
	@Order(100)
	public void finishCleanUp() throws Exception {
		mvc.perform(delete("/approval/delete")
				.contentType(MediaType.APPLICATION_JSON_UTF8))
				.andDo(print())
				.andExpect(status().isOk());
	}
}
